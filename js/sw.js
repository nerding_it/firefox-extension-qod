const URL = 'http://quotes.rest/qod';

const channel = new BroadcastChannel('qod');

channel.addEventListener('message', function (event) {
    switch (event.data.type) {
	case 'LOAD':
	    fetch(URL)
		.then((response) => response.json())
		.then((data) => {
		    const quoteData = data.contents.quotes.pop();
		    const result = {
			content: quoteData.quote,
			background: `url(${quoteData.background})`,
			author: quoteData.author,
			copyright: `© ${data.copyright.year} ${data.copyright.url}`
		    };
		    channel.postMessage({type: 'LOAD_RESULT', data: result});
		});
	    break;
    }
});
