class Loader extends HTMLElement {
    static channel = new BroadcastChannel('qod');

    constructor() {
	super();
	const shadow = this.attachShadow({mode: 'open'});
    }

    onmessage = (event) => {
	switch (event.data.type) {
	    case 'LOAD_RESULT':
		this.shadowRoot.querySelector('div').remove();
		break;
	    default:
		break;
	}
    }

    connectedCallback (event) {
	const container = document.createElement('div');
	const loader = document.createElement('div');
	container.appendChild(loader);
	container.style.display = 'flex';
	container.style.width = '380px';
	container.style.height = '180px';
	container.style.alignItems = 'center';
	container.style.justifyContent = 'center';
	loader.style.border = '16px solid #f3f3f3';
	loader.style.borderTop = '16px solid #3498db';
	loader.style.borderRadius = '50%';
	loader.style.width = '5rem';
	loader.style.height = '5rem';
	loader.style.animation = 'spin 2s linear infinite';
	const animation = document.createElement('style');
	animation.type = 'text/css';
	const animationText = document.createTextNode(`@keyframes spin {
	    0% { transform: rotate(0deg); }
	    100% { transform: rotate(360deg); }
	}`);
	animation.appendChild(animationText);
	this.shadowRoot.appendChild(container);
	this.shadowRoot.appendChild(animation);
	this.constructor.channel.addEventListener('message', this.onmessage);
    }

    disconnectedCallback (event) {
	this.constructor.channel.removeEventListener('message', this.onmessage);
    }    
}

class App extends HTMLElement {
    
    static channel = new BroadcastChannel('qod');
    
    constructor() {
	super();
	const shadow = this.attachShadow({mode: 'open'});
    }

    onmessage = (event) => {
	switch (event.data.type) {
	    case 'LOAD_RESULT':
		const data = event.data.data;
		const container = document.createElement('div');
		const content = document.createElement('p');
		const author = document.createElement('p');
		const copyright = document.createElement('p');
		container.style.overflow = 'hidden';
		container.style.width = '380px';
		container.style.height = '180px';
		container.style.position = 'relative';
		container.style.textAlign = 'center';
		container.style.padding = '10px';
		container.style.color = 'white';

		copyright.style.display = 'block';
		copyright.style.position = 'absolute';
		copyright.style.bottom = 0;
		copyright.style.width = '100%';
		copyright.style.textAlign = 'center';

		author.style.textAlign = 'right';
		
		container.style.backgroundImage = data.background;
		content.textContent = data.content;
		author.textContent = data.author;
		copyright.textContent = data.copyright;
		container.appendChild(content);
		container.appendChild(author);
		container.appendChild(copyright);
		this.shadowRoot.appendChild(container);
	    default:
		break;
	}

    }

    connectedCallback (event) {
	this.constructor.channel.addEventListener('message', this.onmessage);
    }

    disconnectedCallback (event) {
	this.constructor.channel.removeEventListener('message', this.onmessage);
    }
}

customElements.define('app-qod', App);
customElements.define('app-loader', Loader);

document.addEventListener('DOMContentLoaded', function (event) {
    const channel = new BroadcastChannel('qod');
    channel.postMessage({type: 'LOAD'});
});
